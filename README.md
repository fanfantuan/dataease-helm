# Helm Chart 部署 DataEase
## 1. 部署模式
此安装包支持选择部署模式：“精简模式” 和 “集群模式”；

精简模式下仅部署 dataease 和 MySQL，集群模式下将部署 dataease、kettle、mysql。

默认为 simple 模式。

在 values.yaml 中修改：
```
DataEase:
  enabled: true
  engine_mode: cluster或simple
```

## 2. 组件说明
### 2.1 Doris
Doris 在 k8s 环境部署存在缺陷，也不建议将 Doris 部署在 k8s 环境，如果需要数据集定时同步功能，可以将 Doris 外置部署，通过 cluster 模式关联 Doris；

### 2.2 Kettle
Kettle 只有在 cluster 模式下才会部署；

Kettle 默认部署1个副本，如果您想修改它，可以在 values.yaml 中修改：
```
kettle:
  replicas: 1 改为其他数值
```
注意，一个同步任务只能由一个 Kettle 调度，所以增加 Kettle 的数量可以在同一时间内完成更多的同步任务。

### 2.3 DataEase
DataEase 默认有两种外部访问方式：1. ingress， 2. NodePort 

你可以在 values.yaml 中配置 ingress 的开关状态：
```
ingress:
  enabled: true或false
```
也可以配置NodePort的端口：
```
common:
  dataease:
    nodeport_port: 30081 改为其他端口
```
### 2.4 存储
此环境使用 StorageClass 作为共享存储，您可以根据自己的 Kubernetes 环境现有的 StorageClass 修改此名称：
```
common:
  storageClass: dataease 改为你自己环境中的存储类名称
```

## 3. 部署步骤
### 3.1  下载 helm chart 包

访问 https://github.com/mfanoffice/dataease-helm.git

下载 helm chart 包 dataease-1.x.x，放置 Kubernetes 环境的服务器中，并解压压缩包；
```
tar -zxvf dataease-1.x.x.tgz
```

或者git clone 然后自行打包：
```bash
git clone https://github.com/mfanoffice/dataease-helm.git

```

### 3.2 修改配置

解压 helm chart 包后，修改 values.yaml 文件，对存储类、安装模式 或其他参数，按实际使用环境进行修改；

```bash
vi dataease-helm/values.yaml

storageClass: de-nfs 改为其他名称
```

### 3.3 安装


```bash
#创建一个命名空间
kubectl create ns de
#对修改后的 dataease-helm 打包
helm package dataease-helm/
#部署
helm install dataease dataease-1.18.xx.tgz -n de
```

你可以时刻观察 Pod 的状态,如果都为 running 状态则 Pod 启动完成
```bash
kubectl get pod
```

这里需要注意的是，dataease 的 Pod 后台需要完成初始化操作，可以先观察日志等待完成后再继续操作。
```bash
kubectl logs -f dataease -n de
```

升级：
```bash
#直接通过 chart 包升级
helm upgrade dataease dataease-1.18.xx.tgz -n de

#或者通过 values.yaml 升级
helm upgrade dataease -f dataease-helm/values.yaml  dataease-1.18.xx.tgz -n de
```


### 3.4 配置 DataEase


登录 DataEase 的 web 操作界面，完成最后的组件关联
```
# NodePort 方式访问
浏览器访问 http://10.168.1.10:30081
（IP 为 Kubernetes 节点 IP，端口默认 30081）

# Ingress 方式访问
浏览器访问 http://demo.apps.dataease.com
(需要手动做域名映射，默认域名：demo.apps.dataease.com)

用户名：admin
密码： dataease
```

#### 3.4.1 关联 Doris 服务：

系统管理--系统参数--引擎设置
```
Doris地址： 10.168.1.11
#10.168.1.11 为 doris-fe 的 IP 地址，此 IP 也是 Kubernetes 环境中 doris-fe Pod 所在的宿主机 IP

数据库名称： dataease
#在上一步创建的库

用户名： root
密码： Password123@doris
#在上一步创建的 root 密码

Query Port： 9030
#默认端口，前提是您没有修改它

Http Port: 8030
#默认端口
```
填写完成点击 “校验” 成功，点击 “保存” 即可。

#### 3.4.2 关联 Kettle 服务：

系统管理--系统参数--Kettle 设置--添加 Kettle 服务
```
Kettle地址： kettle
#"kettle" 是 Kubernetes 中 kettle 服务名，Pod 网络可以直接解析

端口： 18080
#默认端口，前提是您没有修改它

用户名： cluster
密码： cluster
#默认密码
```
填写完成点击“校验”成功，点击“保存”即可。


完成以上操作您已经在 Kubernetes 中配置完成了 DataEase，接下来请尽情的使用它吧。

最后欢迎大家提 issue!
